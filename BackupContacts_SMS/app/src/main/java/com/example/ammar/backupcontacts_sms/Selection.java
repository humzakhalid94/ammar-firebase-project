package com.example.ammar.backupcontacts_sms;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * Created by ammar on 3/9/16.
 */
public class Selection extends AppCompatActivity implements View.OnClickListener{

    LinearLayout sync, restore, view, delete ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selection);

        initialize();

        sync.setOnClickListener(this);
        view.setOnClickListener(this);
    }

    public void initialize(){
        sync = (LinearLayout) findViewById(R.id.syncContacts);
        restore = (LinearLayout) findViewById(R.id.restore);
        view = (LinearLayout) findViewById(R.id.view);
        delete = (LinearLayout) findViewById(R.id.delete);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.syncContacts:
                Toast.makeText(Selection.this, "Yes", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Selection.this, Contacts.class));

                break;

            case R.id.view:
                Toast.makeText(Selection.this, "Yes", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Selection.this, ViewContacts.class));

                break;
        }
    }
}
